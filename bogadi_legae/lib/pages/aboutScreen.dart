import 'package:flutter/material.dart';

class AboutPage extends StatefulWidget {
  const AboutPage({
    Key? key,
  }) : super(key: key);

  @override
  State<AboutPage> createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
        child: Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: const [
                Padding(
                  padding: EdgeInsets.all(28.0),
                  child: Text(
                    "About Us",
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ),
                Text(
                    "We are a family Business dedicated to catering and hosting... \n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu magna, varius vitae hendrerit ac, pretium sed nulla. Nullam eleifend mauris a risus porta sollicitudin non ut felis. Mauris sed quam ut sapien posuere vulputate. Aliquam condimentum, mauris in eleifend interdum, nisl magna tristique urna, in hendrerit nisi ipsum in ligula. Mauris commodo at leo non porta. \n\nNullam quis ipsum ante. Sed lobortis, dui suscipit volutpat volutpat, lorem enim facilisis libero, at ultrices arcu mi id ligula. Morbi et diam finibus, vulputate turpis sit amet, pellentesque metus.  us"),
                Text('We move under cover and we move as one'),
              ]),
        ),
      ),
    );
  }
}
