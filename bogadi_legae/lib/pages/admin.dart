import 'dart:developer';

import 'package:bogadi_legae/pages/dashboard.dart';
import 'package:bogadi_legae/pages/welcome_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Admin extends StatefulWidget {
  const Admin({Key? key}) : super(key: key);

  @override
  State<Admin> createState() => _AdminState();
}

class _AdminState extends State<Admin> {
  final Stream<QuerySnapshot> _users =
      FirebaseFirestore.instance.collection("bogadi_users").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController nameFieldController = TextEditingController();
    TextEditingController contactnoController = TextEditingController();
    TextEditingController emailFieldController = TextEditingController();
    TextEditingController passWordFieldController = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance.collection("bogadi_users").doc(docId).delete();
      const SnackBar(
        content: Text('Deleted Successfully!'),
      );
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("bogadi_users");

      nameFieldController.text = data["full_name"];
      emailFieldController.text = data["email"];
      contactnoController.text = data["contactno"];
      passWordFieldController.text = data["password"];

      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text("Update"),
          content: Column(mainAxisSize: MainAxisSize.min, children: [
            TextField(
              controller: nameFieldController,
            ),
            TextField(
              controller: emailFieldController,
              enabled: false,
            ),
            TextField(
              controller: passWordFieldController,
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 10.0),
                child: OutlinedButton(
                    onPressed: () {
                      collection.doc(data["doc_id"].toString()).update({
                        "full_name": nameFieldController.text,
                        "email": emailFieldController.text,
                        "contactno": contactnoController.text,
                        "password": passWordFieldController.text,
                      }).onError((error, stackTrace) => const SnackBar(
                          content: Text('Something broke, Please try again!')));
                      Navigator.pop(context);
                    },
                    child: const Text("Update Booking")))
          ]),
        ),
      );
    }

    return StreamBuilder(
        stream: _users,
        builder: (BuildContext context,
            AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
          if (snapshot.hasError) {
            return const Text("Something broke, we are sorry for that!");
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }

          if (snapshot.hasData) {
            return Row(
              children: [
                Expanded(
                    child: SizedBox(
                  height: (MediaQuery.of(context).size.height),
                  width: (MediaQuery.of(context).size.width),
                  child: ListView(
                    children: snapshot.data!.docs
                        .map((DocumentSnapshot documentSnapshot) {
                      Map<String, dynamic> data =
                          documentSnapshot.data() as Map<String, dynamic>;
                      return Column(
                        children: [
                          Card(
                            child: Column(
                              children: [
                                ListTile(
                                  title: Text(data["full_name"]),
                                  subtitle: Text(data["email"]),
                                ),
                                ButtonTheme(
                                    child: ButtonBar(
                                  children: [
                                    OutlinedButton.icon(
                                      onPressed: () {
                                        _update(data);
                                      },
                                      icon: const Icon(Icons.edit),
                                      label: const Text("Edit"),
                                    ),
                                    OutlinedButton.icon(
                                      onPressed: () {
                                        _delete(data["doc_id"]);
                                      },
                                      icon: const Icon(Icons.remove),
                                      label: const Text("Delete"),
                                    )
                                  ],
                                ))
                              ],
                            ),
                          )
                        ],
                      );
                    }).toList(),
                  ),
                ))
              ],
            );
          } else {
            return (const Text("No Data"));
          }
        });
  }
}

class EditProf extends StatefulWidget {
  const EditProf({Key? key}) : super(key: key);

  @override
  State<EditProf> createState() => _EditProfState();
}

class _EditProfState extends State<EditProf> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController contactController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  Future _editProfiles() {
    final name = nameController.text;
    final email = emailController.text;
    final contactno = contactController.text;
    final password = passwordController.text;

    final ref = FirebaseFirestore.instance.collection("bogadi_users").doc();

    return ref.set({
      "full_name": name,
      "email": email,
      "contactno": contactno,
      "password": password,
      "doc_id": ref.id
    }).then((value) => {
          nameController.text = "",
          emailController.text = "",
          contactController.text = '',
          passwordController.text = "",
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 40.0, bottom: 30.0),
              child: Image.asset(
                "assets/images/userProfile.png",
                // height: 160,
                width: 80,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20.0)),
                  ),
                  labelText: "Full Name",
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
              child: TextField(
                controller: emailController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20.0)),
                  ),
                  labelText: "Email",
                  hintText: 'bogadi@bogadi.co.za',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
              child: TextField(
                controller: contactController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20.0)),
                    ),
                    labelText: "Contact Number",
                    hintText: '083 123 4321'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
              child: TextField(
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20.0)),
                  ),
                  labelText: "Password",
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 10.0),
            ),
            TextButton(
              style: ElevatedButton.styleFrom(
                padding:
                    const EdgeInsets.symmetric(horizontal: 50, vertical: 20),

                onPrimary: Theme.of(context).colorScheme.onSecondaryContainer,
                // Background color
                primary: Theme.of(context).colorScheme.secondaryContainer,
              ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
              onPressed: () {
                _editProfiles();
              },
              child: const Text(
                'Submit',
              ),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 30.0),
              child: Text("User Bookings",
                  style: TextStyle(
                      fontSize: 36,
                      fontWeight: FontWeight.w700,
                      decoration: TextDecoration.underline)),
            ),
            const Admin()
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const WelcomeScreen()),
          );
        },
        label: const Text('logout'),
        icon: const Icon(Icons.exit_to_app),
        backgroundColor: Colors.orange,
      ),
    );
  }
}
