import 'package:flutter/material.dart';

import 'dashboard.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Object checkUser() {
      late String email = emailController.text;
      late String password = passwordController.text;

      if (email.isNotEmpty && password.isNotEmpty) {
        return Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const Dashboard()),
        );
      }
      return true;
    }

    return Scaffold(
        // backgroundColor: Colors.white30,
        appBar: AppBar(
          title: const Text("Login"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(28.0),
          child: Column(
            children: [
              Container(
                alignment: Alignment.center,
                child: Image.asset('images/userProfile.png',
                    height: 101, width: 161),
              ),
              const Text(
                'Login',
                style: TextStyle(fontSize: 28, height: 2),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: TextFormField(
                  controller: emailController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20.0)),
                    ),
                    labelText: 'Username',
                    hintText: "username",
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                child: TextFormField(
                  controller: passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20.0)),
                    ),
                    labelText: 'Password',
                    hintText: "password",
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(5, 5, 50, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 50, vertical: 20),
                        onPrimary:
                            Theme.of(context).colorScheme.onSecondaryContainer,
                        // Background color
                        primary:
                            Theme.of(context).colorScheme.secondaryContainer,
                      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text(
                        'Cancel',
                      ),
                    ),
                    TextButton(
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 50, vertical: 20),

                        onPrimary:
                            Theme.of(context).colorScheme.onSecondaryContainer,
                        // Background color
                        primary:
                            Theme.of(context).colorScheme.secondaryContainer,
                      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
                      onPressed: () {
                        checkUser();
                      },
                      child: const Text(
                        'Login',
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
